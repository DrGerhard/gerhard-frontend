import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Admin from './views/Admin.vue'
import CommentView from './views/CommentView.vue'
import NotFound from './views/NotFound.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      meta: {
        title: 'Tohtori Gerhard - Tilaa pullo!',
        metaTags: [
          {
            name: 'description',
            content: 'Mieskuntoa parantava pullo'
          }
        ]
      }
    },
    {
      path: '/admin',
      name: 'admin',
      component: Admin,
      meta: {
        title: 'Tohtori Gerhard - Admin',
      }
    },
    {
      path: '/comment/:id',
      name: 'comment',
      component: CommentView,
      meta: {
        metaTags: [
          {
            name: 'description',
            content: 'Mieskuntoa parantava pullo'
          },
          {
            name: 'twitter:card',
            content: 'summary'
          }
        ]
      }
    },
    {
      path: '*',
      component: NotFound
    }
  ]
})
